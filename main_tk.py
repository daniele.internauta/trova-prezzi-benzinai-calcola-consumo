from tkinter import *
from tkinter.ttk import Progressbar
from tkinter import messagebox as mbox
import re
import pandas as pd
import requests
import json
import os

class BootApp(Tk):
    '''
    classe madre che ha come parametro Tk per inzializzare l'interfaccia grafica 
    '''
    def __init__(self):
        '''
        inzializzo il contenitore grafico Tk (master) con i suoi parametri  
        '''
        Tk.__init__(self)
        self.geometry("340x241")
        self.iconbitmap('img\\italy-icon-0.ico')
        self.title("Calcola consumi")
        self.frames = None
        dato = None
        self.sfoglia_frame(PaginaInizio, dato)
        

    def sfoglia_frame(self, frame_class, dati):
        """
        Definisco un metodo sfoglia che prende come parametri:
        self = che inzializza il contenitore grafico madre 
        fram_class = Nome della classe 
        dati = dato inzializzato a None
        E come risultato crea un frame vuoto, figlio di BootApp 
        """
        new_frame = frame_class(self, dati)
        if self.frames is not None:
            self.frames.destroy()
        self.frames = new_frame
        self.frames.grid()


class PaginaInizio(Frame):

    def __init__(self, master, dato):
        Frame.__init__(self)
        titolo_pi = Label(self, text="Calcola Consumi", font=('Helvetica', 17, "bold"))
        titolo_pi.grid(row=0, columnspan=2, padx=75, pady=15)
        trova_p = Label(self, text="Trova prezzi\nCarburanti",
                        font=('Helvetica', 11, "bold"))

        trova_p.grid(row=1, column=0, pady=5)
        inserisci_p = Label(self, text="Inserisci prezzo\nManualmente",
                            font=('Helvetica', 11, "bold"))

        inserisci_p.grid(row=1, column=1)
        bottone_trova_p = Button(self, text=">>O<<", command=lambda:
                                 master.sfoglia_frame(PaginaSceltaLuogo, dato))
        bottone_trova_p.grid(row=3, column=0)
        bottone_inserisci_p = Button(self, text=">>O<<", command=lambda:
                                     master.sfoglia_frame(PaginaCalcolo, "manuale"))

        bottone_inserisci_p.grid(row=3, column=1, pady=5)


class PaginaCeck(Tk):

    def __init__(self):
        Toplevel.__init__(self)
        self.geometry("340x241")
        self.iconbitmap('img\\italy-icon-0.ico')
        self.title("Update/Create")
        testo_aggiorna = Label(self, text="Controllo File di Sistema")
        testo_aggiorna.grid(row=0, column=0, columnspan=2, pady=3, padx=100)

        etichetta_track = Label(self, text="File Track.json")
        etichetta_track.grid(row=1, column=0, stick=E, pady=3)
        self.barra_track = Progressbar(self, orient="horizontal", length=150,
                                mode="determinate")
        self.barra_track.grid(row=1, column=1, pady=3)
        

        etichetta_regioni = Label(self, text="File Regioni-prov.csv")
        etichetta_regioni.grid(row=2, column=0, stick=E, pady=3)
        self.barra_regioni = Progressbar(self, orient="horizontal", length=150,
                                mode="determinate")
        self.barra_regioni.grid(row=2, column=1, pady=3)

        etichetta_anagrafica = Label(self, text="File Anagrafica.csv")
        etichetta_anagrafica.grid(row=3, column=0, stick=E, pady=3)
        self.barra_anagrafica = Progressbar(self, orient="horizontal", length=150,
                                mode="determinate")
        self.barra_anagrafica.grid(row=3, column=1, pady=3)

        etichetta_prezzi = Label(self, text="File Prezzi.csv")
        etichetta_prezzi.grid(row=4, column=0, stick=E, pady=3)
        self.barra_prezzi = Progressbar(self, orient="horizontal", length=150,
                                   mode="determinate")
        self.barra_prezzi.grid(row=4, column=1, pady=3)

        self.statusbar = Canvas(self, width=335, height=20, bg='#d4d4d4', relief="groove", borderwidth=1)
        self.statusbar.grid(row=5, column=0, columnspan=3, pady=80)
        self.testo_statusbar = self.statusbar.create_text(170, 12,
                                                          fill="darkblue",
                                                          font="Times 12 italic bold", text="")

        testo_ceck_status = "Controllo dei file di sistema"
        self.animazione(testo_ceck_status)
        self.controllo_sys_update()

    def controllo_sys_update(self):
        self.URL_A = 'https://www.mise.gov.it/images/exportCSV/anagrafica_impianti_attivi.csv'
        header_impianti = requests.head(self.URL_A)
        i_grandezza = header_impianti.headers['Content-Length']
        h_data_impianti = header_impianti.headers['Last-Modified']

        self.URL_P = 'https://www.mise.gov.it/images/exportCSV/prezzo_alle_8.csv'
        header_prezzo = requests.head(self.URL_P)
        p_grandezza = header_prezzo.headers['Content-Length']
        h_data_prezzo = header_prezzo.headers['Last-Modified']
        dati_trak_list = [p_grandezza, h_data_prezzo, i_grandezza, h_data_impianti]

        try:
            file_traccia = open('track.json', 'r')
            dati_traccia = json.loads(file_traccia.read())
            file_traccia.close()
            self.controllo_file(self.barra_track)


            if dati_traccia[3] != h_data_impianti:
                self.aggiornamenti("anagrafica", dati_trak_list)
            if dati_traccia[1] != h_data_prezzo:
                self.aggiornamenti("prezzi", dati_trak_list)
        except IOError:
            testo_up_status = ("Primo Avvio! Creati File mancanti!")
            self.animazione(testo_up_status)
            
            file_traccia = open('track.json', 'w')
            val_diz = ["0", "Tue, 15 Sep 2018 05:59:52 GMT",
                       "0", "Tue, 15 Sep 2018 05:59:52 GMT"]
            self.barra_track["value"] = 100
            file_traccia.write(json.dumps(val_diz))
            file_traccia.close()
            self.aggiornamenti("anagrafica", dati_trak_list)
            self.aggiornamenti("prezzi", dati_trak_list)
            
        try:
            file_regioni = open('prov-regioni-provincia.csv', "r")
            file_regioni.close()
            self.controllo_file(self.barra_regioni)
        except IOError:
            self.aggiornamenti("regioni", dati_trak_list)

        try:
            anagrafica_i = open("anagrafica_impianti_attivi.csv", "r")
            anagrafica_i.close()
            self.controllo_file(self.barra_anagrafica)
        except IOError:

            self.aggiornamenti("anagrafica", dati_trak_list)

        try:
            prezzo8 = open("prezzo_alle_8.csv", "r")
            prezzo8.close()
            self.controllo_file( self.barra_prezzi)
        except IOError:
            self.aggiornamenti("prezzi", dati_trak_list)

    def animazione(self, testo):
        delta = 50 
        delay = 0
        for i in range(len(testo) + 1):
            s = testo[:i]
            update_testo = lambda s=s: self.statusbar.itemconfigure(self.testo_statusbar, text=s)
            self.statusbar.after(delay, update_testo)
            delay += delta

    def aggiornamenti(self, tipo, lista_track):
        if tipo == "regioni":
            link_regioni = ('http://salvadanaiodellecrypto.altervista.org/img/prov-regioni-provincia.csv')
            
            with open('prov-regioni-provincia.csv', "wb") as file_regioni:
                n_in2 = 0
                www_regioni = requests.get(link_regioni, allow_redirects=True, stream=True)
                if www_regioni.status_code == 200:
                    for byte_regioni in www_regioni.iter_content(int(2448/100)):
                        n_in2 += 1
                        file_regioni.write(byte_regioni)
                        self.barra_regioni['value'] = n_in2-1
                        self.update()
            file_regioni.close()

        if tipo == "anagrafica":
            with open('anagrafica_impianti_attivi.csv', 'wb') as file_anagrafica:
                n_in1 = 0
                www_impianti = requests.get(self.URL_A, allow_redirects=True, stream=True)

                if www_impianti.status_code == 200:
                    for chunk in www_impianti.iter_content(int(int(lista_track[2])/100)):
                        n_in1 += 1
                        file_anagrafica.write(chunk)
                        self.barra_anagrafica['value'] = (n_in1-1)
                        self.update()
                with open('track.json', 'r+') as track_json:
                    dati_json = json.load(track_json)
                    dati_json[2] = lista_track[2]
                    dati_json[3] = lista_track[3]
                    track_json.seek(0)
                    track_json.write(json.dumps(dati_json))
                    track_json.truncate()

        if tipo == "prezzi":
            with open('prezzo_alle_8.csv', 'wb') as file_p8:
                n_in = 0
                www_prezzo8 = requests.get(self.URL_P, allow_redirects=True, stream=True)
                if www_prezzo8.status_code == 200:
                    for chunk in www_prezzo8.iter_content((int(int(lista_track[0])/100))):
                        n_in += 1
                        file_p8.write(chunk)
                        self.barra_prezzi['value'] = n_in-1
                        self.update()
                else:
                    print("problemi di connessione")
            with open('track.json', 'r+') as track_json:
                    dati_json = json.load(track_json)
                    dati_json[0] = lista_track[0]
                    dati_json[1] = lista_track[1]
                    track_json.seek(0)
                    track_json.write(json.dumps(dati_json))
                    track_json.truncate()

    def controllo_file(self, barra):
            for increment_b_t in range(101):
                barra["value"] = increment_b_t
                barra.update()


class PaginaSceltaLuogo(Frame):

    def __init__(self, master, dato):
        Frame.__init__(self)
        anagrafica_impianti_csv = pd.read_csv("anagrafica_impianti_attivi.csv",
                                              encoding="latin-1", sep=";", header=1)

        regioni_provincie_csv = pd.read_csv("prov-regioni-provincia.csv", encoding="latin-1",
                                            sep=",", names=["Prov", "Regione", "Provincia"])

        self.anagrafica_regioni = pd.merge(anagrafica_impianti_csv, regioni_provincie_csv,
                                           on='Provincia')

        self.testo_scelta = Label(self, text="Scegli la regione")
        self.testo_scelta.grid(row=0, column=0,  padx=109, columnspan=2)
        selezione_regione = self.anagrafica_regioni["Regione"]
        self.scelta_lista = Listbox(self)
        s_r_pulisci_duplicati = selezione_regione.drop_duplicates()
        for n, reg_p in enumerate(s_r_pulisci_duplicati):
            regioni = self.scelta_lista.insert(n, reg_p)
        self.scelta_lista.grid(row=1, column=0, columnspan=2)
        self.bottone_i_lista = Button(self, text="<<", command=lambda:
                                      master.sfoglia_frame(PaginaInizio, dato))

        self.bottone_i_lista.grid(row=2, column=0)
        self.bottone_a_lista = Button(self, text=">>", command=lambda:
                                      self.controllo_l_scelta(master, dato))

        self.bottone_a_lista.grid(row=2, column=1)

    def controllo_l_scelta(self, master, dato):

        if self.scelta_lista.curselection() == tuple():
            mbox.showwarning("Attenzione", "Fare la propria selezione\nPrima di procedere!")
            self.destroy()
            master.sfoglia_frame(PaginaSceltaLuogo, dato)
        elif type(self.scelta_lista.get(ANCHOR)) == tuple:
            dato = self.scelta_lista.get(ANCHOR)[2]
            master.sfoglia_frame(PaginaCalcolo, dato)
        else:
            self.definisci_lista(master)

    def definisci_lista(self, master):
        dato = None
        stringa_scelta = self.scelta_lista.get(ANCHOR)
        regione_mach = self.anagrafica_regioni["Regione"] == stringa_scelta
        lista_provincie = self.anagrafica_regioni[regione_mach]["Prov"]
        lista_provincie_singole = lista_provincie.drop_duplicates()
        if(len(lista_provincie_singole.values)) > 0:
            self.testo_scelta.config(text="Scegli la provincia")
            self.scelta_lista.delete(0, END)
            for index, r_m in lista_provincie_singole.items():
                self.scelta_lista.insert(index, r_m)

        provincia_mach = self.anagrafica_regioni["Prov"] == stringa_scelta
        lista_comuni = self.anagrafica_regioni[provincia_mach]["Comune"]
        lista_comuni_singoli = lista_comuni.drop_duplicates()
        if(len(lista_comuni_singoli.values)) > 0:
            self.testo_scelta.config(text="Scegli il comune")
            self.scelta_lista.delete(0, END)
            for index, r_m in lista_comuni_singoli.items():
                self.scelta_lista.insert(index, r_m)

        prezzi_i = pd.read_csv("prezzo_alle_8.csv", encoding="latin-1", sep=";", header=1)
        anagrafica_regioni_prezzi = pd.merge(self.anagrafica_regioni, prezzi_i, on="idImpianto")
        comune_mach = anagrafica_regioni_prezzi["Comune"] == stringa_scelta
        lista_impianti = anagrafica_regioni_prezzi[comune_mach].to_records()

        if(len(lista_impianti)) > 0:
            self.testo_scelta.config(text="Scegli il Carburante")
            self.scelta_lista.delete(0, END)
            self.scelta_lista.config(width=55)
            for index, r_m in enumerate(lista_impianti):
                tipo_carburante = r_m["descCarburante"]
                prezzo = round(r_m["prezzo"], 4)
                indirizzo = r_m["Indirizzo"]
                bandiera = r_m["Bandiera"]
                lista_b_tc_p_i = [bandiera, tipo_carburante, prezzo, indirizzo]
                self.scelta_lista.insert(index, lista_b_tc_p_i)
            self.bottone_a_lista.config(command=lambda: self.controllo_l_scelta(master, dato))

        self.bottone_i_lista.config(command=lambda: [self.scelta_lista.destroy(),
                                    self.testo_scelta.destroy(),
                                    master.sfoglia_frame(PaginaSceltaLuogo, dato)])


class PaginaCalcolo(Frame):

    def __init__(self, master, dato):
        Frame.__init__(self)
        if dato == "manuale":
            self.prezzo_b = Entry(self)
            self.prezzo_b.grid(row=1, column=1)
        else:
            self.prezzo_b = Entry(self)
            self.prezzo_b.insert(0, dato)
            self.prezzo_b.configure(state='disabled')
            self.prezzo_b.grid(row=1, column=1)

        self.titolo_var = Label(self, text="Calcola quanto spenderai")
        self.titolo_var.grid(row=0, column=0, columnspan=2, padx=100, pady=12)
        etichetta_pb = Label(self, text="Prezzo benzina:")
        etichetta_pb.grid(row=1, column=0)
        etichetta_km = Label(self, text="Quanti Km devi percorrere?")
        etichetta_km.grid(row=2, column=0)
        self.km_in = Entry(self)
        self.km_in.grid(row=2, column=1)
        self.km_x_l_in = Entry(self)
        self.km_x_l_in.grid(row=3, column=1)
        butt_avanti = Button(self, text=">>", command=lambda: self.calcola())
        butt_avanti.grid(row=4, column=1)
        butt_indietro = Button(self, text="<<", command=lambda:
                               master.sfoglia_frame(PaginaInizio, dato))

        butt_indietro.grid(row=4, column=0)
        etichetta_km_x_l = Label(self, text="inserisci quanti"
                                            "\nKm per 1 Litro consuma"
                                            "\nla tua macchina:")

        etichetta_km_x_l.grid(row=3, column=0)
        self.risultato = Label(self, text=" ")
        self.risultato.grid(row=5, column=0, columnspan=2, pady=13)

    def calcola(self):
        self.risultato['text'] = ""
        m_km = re.match(r"^[0-9\.]*$", self.km_in.get())
        m_km_x_l = re.match(r"^[0-9\.]*$", self.km_x_l_in.get())
        m_p_b_m = re.match(r"^[0-9\.]*$", self.prezzo_b.get())
        if (m_km is None or m_km_x_l is None or m_p_b_m is None or
           m_km.group() == "" or m_km_x_l.group() == "" or m_p_b_m.group() == ""):

            self.risultato.config(text="Compila tutti i campi"
                                       "\ninserire solo numeri"
                                       "\ninteri o decimali",
                                       font=("Verdana", 12, "bold"),
                                       fg='red')

        else:
            km = float(self.km_in.get())
            kml = float(self.km_x_l_in.get())
            totale = round((km/kml)*float(m_p_b_m.group()), 2)
            self.risultato.config(text="Spenderai "+str(totale)+"€",
                                  font=("Verdana", 17, "bold"),
                                  fg="#1f7a1f")
            self.prezzo_b.configure(state='normal')


app = BootApp()
PaginaCeck()
app.mainloop()
