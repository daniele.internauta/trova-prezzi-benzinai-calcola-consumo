# Trova Benzinai - Calcola Consumi

Il programma scarica e aggiorna automaticamente gli impianti nel territorio Italiano e i prezzi carburante dal sito [Governativo Mise](https://www.mise.gov.it/index.php/it/open-data/elenco-dataset/2032336-carburanti-prezzi-praticati-e-anagrafica-degli-impianti)  
Da la possibilità di fare una scelta dell' impiantianto desiderato ,  
calcolare quanto si consuma per fare Tot di Km inserendo :  
 - il prezzo scelto in precedenza o scrivendolo manualmente 
 - Il consumo della propria auto in Km per ogni litro 
 - i Km da percorrere

## Moduli usati

il File requisiti.txt contiene i moduli usati basta fare un : `pip install -r requisiti.txt -y`
i moduli Tkinter, json dovrebbero essere installati di default , in caso contrario installarli

## Sviluppo & Obbiettivi

Questo programma è sviluppato come esame personale per l'apprendimento del linguaggio python.  
Reso pubblico per avere dei feedback , suggerimenti e segnalazioni di errore .  
Io sviluppo è inziato alla fine  2018 , dove inzialmente i dati sugli impianti e i prezzi li prelevavo da altri siti con il metodo dello scarping , un metodo poco affidabile e decisamente piu lento, per questo tipo di dati .  
Obbiettivo primario (ancora da raggiungere):  
Creare un software per PC con veste grafica che permetta di calcolare il prezzo di consumo di carburante per i km percorsi inserendo i dati manualmente o scgliendo dai dati aggiornati quotidianamente via web ,sia sui prezzi e stazioni di servizio , sia per il consumo dei vari modelli d'auto (km per ogni litro)

